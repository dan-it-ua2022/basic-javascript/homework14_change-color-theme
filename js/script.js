const styleBtnContainer = document.querySelector('.change-style-btn');
const defaultStyleBtn = document.querySelector('[data-style-color=default]');
const blueStyleBtn = document.querySelector('[data-style-color=blue]');
const nameKeyForStorage = 'nameStyle';
const nameStyleActive = 'active';

defaultStyleBtn.addEventListener('click', (e) => {
  clearActiveBtn();
  setActiveBtn(e.target);
  clearStyle();
  setStyleFromStorage(e.target.dataset.styleColor);
});

blueStyleBtn.addEventListener('click', (e) => {
  {
    clearActiveBtn();
    setActiveBtn(e.target);
    // вот тут например мне нужно вызывать функцию setStyle(userSetStyle)
    // но т.к. userSetStyle = 'blue' - строка, функция не пашет как надо
    let userSetStyle = e.target.dataset.styleColor;
    // setStyle(userSetStyle);
    setStyle(blue);
    setStyleFromStorage(e.target.dataset.styleColor);
  }
});

const blue = {
  '.header a': {
    color: '#00e2cb',
  },
  '.main a .item-link-tour': {
    color: '#00e2cb',
    fontSize: '20px',
  },
  '.item-detail': {
    background: '#00e2cb',
  },
  '.item-link-img': {
    borderImage: 'linear-gradient(-145grad, #00e2cb, #09ff00) 2',
  },
  '.footer': {
    color: '#00e2cb',
    fontSize: '16px',
  },
};

const red = {};

const defaultStyle = [
  '.header a',
  '.main a .item-link-tour',
  '.item-detail',
  '.item-link-img',
  '.footer',
];

/*************************************/
let nameStyle = getStyleFromStorage();
if (!nameStyle) {
  setStyleFromStorage('default');
} else {
  switch (nameStyle) {
    case 'blue':
      blueStyleBtn.click();
      break;
    case 'red':
      break;
    default:
      defaultStyleBtn.click();
  }
}
/*************************************/

function setActiveBtn(item) {
  item.classList.add(nameStyleActive);
}

function clearActiveBtn() {
  [...styleBtnContainer.children].forEach((btn) => {
    btn.classList.remove(nameStyleActive);
  });
}

function setStyleFromStorage(valueStorageStyleName) {
  localStorage.setItem(nameKeyForStorage, valueStorageStyleName);
}

function getStyleFromStorage() {
  return localStorage.getItem(nameKeyForStorage);
}

function setStyle(objStyle) {
  let itemsCssName = Object.keys(objStyle);

  itemsCssName.forEach((nameCssClassInObj) => {
    let cssProperties = Object.entries(objStyle[nameCssClassInObj]);
    let items = document.querySelectorAll(nameCssClassInObj);

    if (items.length) {
      setCssPropertiesForItems(items, cssProperties);
    }
  });
}

function setCssPropertiesForItems(items, cssProperties) {
  items.forEach((item) => {
    cssProperties.forEach((cssStyle) => {
      item.style[cssStyle[0]] = cssStyle[1];
    });
  });
}

function clearStyle() {
  defaultStyle.forEach((nameCssClassInObj) => {
    let items = document.querySelectorAll(nameCssClassInObj);

    if (items.length) {
      items.forEach((item) => {
        item.removeAttribute('style');
      });
    }
  });
}
